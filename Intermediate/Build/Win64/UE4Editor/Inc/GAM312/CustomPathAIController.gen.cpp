// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAM312/CustomPathAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCustomPathAIController() {}
// Cross Module References
	GAM312_API UClass* Z_Construct_UClass_ACustomPathAIController_NoRegister();
	GAM312_API UClass* Z_Construct_UClass_ACustomPathAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_GAM312();
// End Cross Module References
	void ACustomPathAIController::StaticRegisterNativesACustomPathAIController()
	{
	}
	UClass* Z_Construct_UClass_ACustomPathAIController_NoRegister()
	{
		return ACustomPathAIController::StaticClass();
	}
	struct Z_Construct_UClass_ACustomPathAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACustomPathAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_GAM312,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACustomPathAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CustomPathAIController.h" },
		{ "ModuleRelativePath", "CustomPathAIController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACustomPathAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACustomPathAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACustomPathAIController_Statics::ClassParams = {
		&ACustomPathAIController::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACustomPathAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACustomPathAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACustomPathAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACustomPathAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACustomPathAIController, 3095609432);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACustomPathAIController(Z_Construct_UClass_ACustomPathAIController, &ACustomPathAIController::StaticClass, TEXT("/Script/GAM312"), TEXT("ACustomPathAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACustomPathAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
