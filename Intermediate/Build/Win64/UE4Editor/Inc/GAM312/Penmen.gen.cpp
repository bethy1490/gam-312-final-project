// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAM312/Penmen.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePenmen() {}
// Cross Module References
	GAM312_API UClass* Z_Construct_UClass_APenmen_NoRegister();
	GAM312_API UClass* Z_Construct_UClass_APenmen();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_GAM312();
	GAM312_API UFunction* Z_Construct_UFunction_APenmen_GetCurrentStamina();
	GAM312_API UFunction* Z_Construct_UFunction_APenmen_GetMaxStamina();
	GAM312_API UFunction* Z_Construct_UFunction_APenmen_UpdateCurrentStamina();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void APenmen::StaticRegisterNativesAPenmen()
	{
		UClass* Class = APenmen::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentStamina", &APenmen::execGetCurrentStamina },
			{ "GetMaxStamina", &APenmen::execGetMaxStamina },
			{ "UpdateCurrentStamina", &APenmen::execUpdateCurrentStamina },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics
	{
		struct Penmen_eventGetCurrentStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(Penmen_eventGetCurrentStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Stamina" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "function to get current stamina" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APenmen, "GetCurrentStamina", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(Penmen_eventGetCurrentStamina_Parms), Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APenmen_GetCurrentStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APenmen_GetCurrentStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APenmen_GetMaxStamina_Statics
	{
		struct Penmen_eventGetMaxStamina_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(Penmen_eventGetMaxStamina_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Stamina" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "function to get max stamina" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APenmen, "GetMaxStamina", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14020401, sizeof(Penmen_eventGetMaxStamina_Parms), Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APenmen_GetMaxStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APenmen_GetMaxStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics
	{
		struct Penmen_eventUpdateCurrentStamina_Parms
		{
			float Stamina;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Stamina;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::NewProp_Stamina = { UE4CodeGen_Private::EPropertyClass::Float, "Stamina", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Penmen_eventUpdateCurrentStamina_Parms, Stamina), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::NewProp_Stamina,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::Function_MetaDataParams[] = {
		{ "Category", "Stamina" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "function to update stamina" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APenmen, "UpdateCurrentStamina", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Penmen_eventUpdateCurrentStamina_Parms), Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APenmen_UpdateCurrentStamina()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APenmen_UpdateCurrentStamina_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APenmen_NoRegister()
	{
		return APenmen::StaticClass();
	}
	struct Z_Construct_UClass_APenmen_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpringArm_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpringArm;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThirdPersonCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThirdPersonCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstPersonCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FirstPersonCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentStamina_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentStamina;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxStamina_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxStamina;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APenmen_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_GAM312,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APenmen_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APenmen_GetCurrentStamina, "GetCurrentStamina" }, // 2491338403
		{ &Z_Construct_UFunction_APenmen_GetMaxStamina, "GetMaxStamina" }, // 1066884991
		{ &Z_Construct_UFunction_APenmen_UpdateCurrentStamina, "UpdateCurrentStamina" }, // 3347462571
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Penmen.h" },
		{ "ModuleRelativePath", "Penmen.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::NewProp_SpringArm_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "Add third person camera spring arm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APenmen_Statics::NewProp_SpringArm = { UE4CodeGen_Private::EPropertyClass::Object, "SpringArm", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00100000000a001d, 1, nullptr, STRUCT_OFFSET(APenmen, SpringArm), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::NewProp_SpringArm_MetaData, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::NewProp_SpringArm_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::NewProp_ThirdPersonCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "add third person camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APenmen_Statics::NewProp_ThirdPersonCamera = { UE4CodeGen_Private::EPropertyClass::Object, "ThirdPersonCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00100000000a001d, 1, nullptr, STRUCT_OFFSET(APenmen, ThirdPersonCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::NewProp_ThirdPersonCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::NewProp_ThirdPersonCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::NewProp_FirstPersonCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "add first person camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APenmen_Statics::NewProp_FirstPersonCamera = { UE4CodeGen_Private::EPropertyClass::Object, "FirstPersonCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00100000000a001d, 1, nullptr, STRUCT_OFFSET(APenmen, FirstPersonCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::NewProp_FirstPersonCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::NewProp_FirstPersonCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::NewProp_CurrentStamina_MetaData[] = {
		{ "Category", "Stamina" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "create variable for CurrentStamina" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APenmen_Statics::NewProp_CurrentStamina = { UE4CodeGen_Private::EPropertyClass::Float, "CurrentStamina", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(APenmen, CurrentStamina), METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::NewProp_CurrentStamina_MetaData, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::NewProp_CurrentStamina_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmen_Statics::NewProp_MaxStamina_MetaData[] = {
		{ "Category", "Stamina" },
		{ "ModuleRelativePath", "Penmen.h" },
		{ "ToolTip", "create variable for MaxStamina" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APenmen_Statics::NewProp_MaxStamina = { UE4CodeGen_Private::EPropertyClass::Float, "MaxStamina", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000020001, 1, nullptr, STRUCT_OFFSET(APenmen, MaxStamina), METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::NewProp_MaxStamina_MetaData, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::NewProp_MaxStamina_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APenmen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APenmen_Statics::NewProp_SpringArm,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APenmen_Statics::NewProp_ThirdPersonCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APenmen_Statics::NewProp_FirstPersonCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APenmen_Statics::NewProp_CurrentStamina,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APenmen_Statics::NewProp_MaxStamina,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APenmen_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APenmen>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APenmen_Statics::ClassParams = {
		&APenmen::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_APenmen_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_APenmen_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APenmen_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APenmen()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APenmen_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APenmen, 3891371078);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APenmen(Z_Construct_UClass_APenmen, &APenmen::StaticClass, TEXT("/Script/GAM312"), TEXT("APenmen"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APenmen);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
