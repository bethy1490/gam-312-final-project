// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAM312/ComponentPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComponentPawn() {}
// Cross Module References
	GAM312_API UClass* Z_Construct_UClass_AComponentPawn_NoRegister();
	GAM312_API UClass* Z_Construct_UClass_AComponentPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_GAM312();
// End Cross Module References
	void AComponentPawn::StaticRegisterNativesAComponentPawn()
	{
	}
	UClass* Z_Construct_UClass_AComponentPawn_NoRegister()
	{
		return AComponentPawn::StaticClass();
	}
	struct Z_Construct_UClass_AComponentPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AComponentPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_GAM312,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AComponentPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "ComponentPawn.h" },
		{ "ModuleRelativePath", "ComponentPawn.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AComponentPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AComponentPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AComponentPawn_Statics::ClassParams = {
		&AComponentPawn::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AComponentPawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AComponentPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AComponentPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AComponentPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AComponentPawn, 2827381208);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AComponentPawn(Z_Construct_UClass_AComponentPawn, &AComponentPawn::StaticClass, TEXT("/Script/GAM312"), TEXT("AComponentPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AComponentPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
