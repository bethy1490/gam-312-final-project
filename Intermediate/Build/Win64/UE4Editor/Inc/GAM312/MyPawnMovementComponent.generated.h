// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_MyPawnMovementComponent_generated_h
#error "MyPawnMovementComponent.generated.h already included, missing '#pragma once' in MyPawnMovementComponent.h"
#endif
#define GAM312_MyPawnMovementComponent_generated_h

#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyPawnMovementComponent(); \
	friend struct Z_Construct_UClass_UMyPawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyPawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(UMyPawnMovementComponent)


#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyPawnMovementComponent(); \
	friend struct Z_Construct_UClass_UMyPawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyPawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(UMyPawnMovementComponent)


#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyPawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyPawnMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyPawnMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyPawnMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyPawnMovementComponent(UMyPawnMovementComponent&&); \
	NO_API UMyPawnMovementComponent(const UMyPawnMovementComponent&); \
public:


#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyPawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyPawnMovementComponent(UMyPawnMovementComponent&&); \
	NO_API UMyPawnMovementComponent(const UMyPawnMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyPawnMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyPawnMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyPawnMovementComponent)


#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_12_PROLOG
#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_INCLASS \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_MyPawnMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
