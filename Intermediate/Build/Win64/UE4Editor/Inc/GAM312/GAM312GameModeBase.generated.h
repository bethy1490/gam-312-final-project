// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_GAM312GameModeBase_generated_h
#error "GAM312GameModeBase.generated.h already included, missing '#pragma once' in GAM312GameModeBase.h"
#endif
#define GAM312_GAM312GameModeBase_generated_h

#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM312GameModeBase(); \
	friend struct Z_Construct_UClass_AGAM312GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGAM312GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGAM312GameModeBase)


#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGAM312GameModeBase(); \
	friend struct Z_Construct_UClass_AGAM312GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AGAM312GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGAM312GameModeBase)


#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM312GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM312GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312GameModeBase(AGAM312GameModeBase&&); \
	NO_API AGAM312GameModeBase(const AGAM312GameModeBase&); \
public:


#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM312GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312GameModeBase(AGAM312GameModeBase&&); \
	NO_API AGAM312GameModeBase(const AGAM312GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM312GameModeBase)


#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_12_PROLOG
#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_INCLASS \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GAM312GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_GAM312GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
