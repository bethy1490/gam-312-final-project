// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATargetPoint;
#ifdef GAM312_EnemyAIController_generated_h
#error "EnemyAIController.generated.h already included, missing '#pragma once' in EnemyAIController.h"
#endif
#define GAM312_EnemyAIController_generated_h

#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGoToNextWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToNextWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGoToNextWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToNextWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyAIController(); \
	friend struct Z_Construct_UClass_AEnemyAIController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAIController)


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyAIController(); \
	friend struct Z_Construct_UClass_AEnemyAIController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAIController)


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAIController(AEnemyAIController&&); \
	NO_API AEnemyAIController(const AEnemyAIController&); \
public:


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAIController(AEnemyAIController&&); \
	NO_API AEnemyAIController(const AEnemyAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAIController)


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Waypoints() { return STRUCT_OFFSET(AEnemyAIController, Waypoints); }


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_15_PROLOG
#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_INCLASS \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_EnemyAIController_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_EnemyAIController_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_EnemyAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
