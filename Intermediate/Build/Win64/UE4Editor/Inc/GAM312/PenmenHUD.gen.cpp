// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAM312/PenmenHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePenmenHUD() {}
// Cross Module References
	GAM312_API UClass* Z_Construct_UClass_APenmenHUD_NoRegister();
	GAM312_API UClass* Z_Construct_UClass_APenmenHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_GAM312();
// End Cross Module References
	void APenmenHUD::StaticRegisterNativesAPenmenHUD()
	{
	}
	UClass* Z_Construct_UClass_APenmenHUD_NoRegister()
	{
		return APenmenHUD::StaticClass();
	}
	struct Z_Construct_UClass_APenmenHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APenmenHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_GAM312,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APenmenHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "PenmenHUD.h" },
		{ "ModuleRelativePath", "PenmenHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APenmenHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APenmenHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APenmenHUD_Statics::ClassParams = {
		&APenmenHUD::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002ACu,
		nullptr, 0,
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_APenmenHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APenmenHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APenmenHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APenmenHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APenmenHUD, 2764113862);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APenmenHUD(Z_Construct_UClass_APenmenHUD, &APenmenHUD::StaticClass, TEXT("/Script/GAM312"), TEXT("APenmenHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APenmenHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
