// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_Penmen_generated_h
#error "Penmen.generated.h already included, missing '#pragma once' in Penmen.h"
#endif
#define GAM312_Penmen_generated_h

#define gam_312_final_project_Source_GAM312_Penmen_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMaxStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMaxStamina(); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_Penmen_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMaxStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMaxStamina(); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_Penmen_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPenmen(); \
	friend struct Z_Construct_UClass_APenmen_Statics; \
public: \
	DECLARE_CLASS(APenmen, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(APenmen)


#define gam_312_final_project_Source_GAM312_Penmen_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAPenmen(); \
	friend struct Z_Construct_UClass_APenmen_Statics; \
public: \
	DECLARE_CLASS(APenmen, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(APenmen)


#define gam_312_final_project_Source_GAM312_Penmen_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APenmen(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APenmen) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APenmen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APenmen); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APenmen(APenmen&&); \
	NO_API APenmen(const APenmen&); \
public:


#define gam_312_final_project_Source_GAM312_Penmen_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APenmen(APenmen&&); \
	NO_API APenmen(const APenmen&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APenmen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APenmen); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APenmen)


#define gam_312_final_project_Source_GAM312_Penmen_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxStamina() { return STRUCT_OFFSET(APenmen, MaxStamina); } \
	FORCEINLINE static uint32 __PPO__CurrentStamina() { return STRUCT_OFFSET(APenmen, CurrentStamina); }


#define gam_312_final_project_Source_GAM312_Penmen_h_16_PROLOG
#define gam_312_final_project_Source_GAM312_Penmen_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_Penmen_h_19_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_Penmen_h_19_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_Penmen_h_19_INCLASS \
	gam_312_final_project_Source_GAM312_Penmen_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_Penmen_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_Penmen_h_19_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_Penmen_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_Penmen_h_19_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_Penmen_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_Penmen_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
