// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_ComponentPawn_generated_h
#error "ComponentPawn.generated.h already included, missing '#pragma once' in ComponentPawn.h"
#endif
#define GAM312_ComponentPawn_generated_h

#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAComponentPawn(); \
	friend struct Z_Construct_UClass_AComponentPawn_Statics; \
public: \
	DECLARE_CLASS(AComponentPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AComponentPawn)


#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAComponentPawn(); \
	friend struct Z_Construct_UClass_AComponentPawn_Statics; \
public: \
	DECLARE_CLASS(AComponentPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AComponentPawn)


#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AComponentPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AComponentPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AComponentPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AComponentPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AComponentPawn(AComponentPawn&&); \
	NO_API AComponentPawn(const AComponentPawn&); \
public:


#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AComponentPawn(AComponentPawn&&); \
	NO_API AComponentPawn(const AComponentPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AComponentPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AComponentPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AComponentPawn)


#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_ComponentPawn_h_9_PROLOG
#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_INCLASS \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_ComponentPawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_ComponentPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_ComponentPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
