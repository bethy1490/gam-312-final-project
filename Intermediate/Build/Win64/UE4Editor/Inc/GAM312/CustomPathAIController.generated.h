// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_CustomPathAIController_generated_h
#error "CustomPathAIController.generated.h already included, missing '#pragma once' in CustomPathAIController.h"
#endif
#define GAM312_CustomPathAIController_generated_h

#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACustomPathAIController(); \
	friend struct Z_Construct_UClass_ACustomPathAIController_Statics; \
public: \
	DECLARE_CLASS(ACustomPathAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(ACustomPathAIController)


#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_INCLASS \
private: \
	static void StaticRegisterNativesACustomPathAIController(); \
	friend struct Z_Construct_UClass_ACustomPathAIController_Statics; \
public: \
	DECLARE_CLASS(ACustomPathAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(ACustomPathAIController)


#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACustomPathAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACustomPathAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACustomPathAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACustomPathAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACustomPathAIController(ACustomPathAIController&&); \
	NO_API ACustomPathAIController(const ACustomPathAIController&); \
public:


#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACustomPathAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACustomPathAIController(ACustomPathAIController&&); \
	NO_API ACustomPathAIController(const ACustomPathAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACustomPathAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACustomPathAIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACustomPathAIController)


#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_15_PROLOG
#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_INCLASS \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_CustomPathAIController_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_CustomPathAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
