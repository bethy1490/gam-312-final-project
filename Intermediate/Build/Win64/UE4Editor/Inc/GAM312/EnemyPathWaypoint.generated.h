// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_EnemyPathWaypoint_generated_h
#error "EnemyPathWaypoint.generated.h already included, missing '#pragma once' in EnemyPathWaypoint.h"
#endif
#define GAM312_EnemyPathWaypoint_generated_h

#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyPathWaypoint(); \
	friend struct Z_Construct_UClass_AEnemyPathWaypoint_Statics; \
public: \
	DECLARE_CLASS(AEnemyPathWaypoint, AStaticMeshActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AEnemyPathWaypoint)


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyPathWaypoint(); \
	friend struct Z_Construct_UClass_AEnemyPathWaypoint_Statics; \
public: \
	DECLARE_CLASS(AEnemyPathWaypoint, AStaticMeshActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AEnemyPathWaypoint)


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyPathWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyPathWaypoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyPathWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyPathWaypoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyPathWaypoint(AEnemyPathWaypoint&&); \
	NO_API AEnemyPathWaypoint(const AEnemyPathWaypoint&); \
public:


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyPathWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyPathWaypoint(AEnemyPathWaypoint&&); \
	NO_API AEnemyPathWaypoint(const AEnemyPathWaypoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyPathWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyPathWaypoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyPathWaypoint)


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PathWaypointOrder() { return STRUCT_OFFSET(AEnemyPathWaypoint, PathWaypointOrder); }


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_13_PROLOG
#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_INCLASS \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_EnemyPathWaypoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
