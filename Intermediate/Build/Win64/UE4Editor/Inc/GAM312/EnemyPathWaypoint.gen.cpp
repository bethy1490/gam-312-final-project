// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAM312/EnemyPathWaypoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyPathWaypoint() {}
// Cross Module References
	GAM312_API UClass* Z_Construct_UClass_AEnemyPathWaypoint_NoRegister();
	GAM312_API UClass* Z_Construct_UClass_AEnemyPathWaypoint();
	ENGINE_API UClass* Z_Construct_UClass_AStaticMeshActor();
	UPackage* Z_Construct_UPackage__Script_GAM312();
// End Cross Module References
	void AEnemyPathWaypoint::StaticRegisterNativesAEnemyPathWaypoint()
	{
	}
	UClass* Z_Construct_UClass_AEnemyPathWaypoint_NoRegister()
	{
		return AEnemyPathWaypoint::StaticClass();
	}
	struct Z_Construct_UClass_AEnemyPathWaypoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathWaypointOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_PathWaypointOrder;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEnemyPathWaypoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AStaticMeshActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GAM312,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyPathWaypoint_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input" },
		{ "IncludePath", "EnemyPathWaypoint.h" },
		{ "ModuleRelativePath", "EnemyPathWaypoint.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyPathWaypoint_Statics::NewProp_PathWaypointOrder_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "EnemyPathWaypoint" },
		{ "ModuleRelativePath", "EnemyPathWaypoint.h" },
		{ "ToolTip", "declare uproperty variable to keep track of order of waypoints in path" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AEnemyPathWaypoint_Statics::NewProp_PathWaypointOrder = { UE4CodeGen_Private::EPropertyClass::Int, "PathWaypointOrder", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000001, 1, nullptr, STRUCT_OFFSET(AEnemyPathWaypoint, PathWaypointOrder), METADATA_PARAMS(Z_Construct_UClass_AEnemyPathWaypoint_Statics::NewProp_PathWaypointOrder_MetaData, ARRAY_COUNT(Z_Construct_UClass_AEnemyPathWaypoint_Statics::NewProp_PathWaypointOrder_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AEnemyPathWaypoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyPathWaypoint_Statics::NewProp_PathWaypointOrder,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEnemyPathWaypoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEnemyPathWaypoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEnemyPathWaypoint_Statics::ClassParams = {
		&AEnemyPathWaypoint::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AEnemyPathWaypoint_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AEnemyPathWaypoint_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AEnemyPathWaypoint_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AEnemyPathWaypoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEnemyPathWaypoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEnemyPathWaypoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEnemyPathWaypoint, 3725171904);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEnemyPathWaypoint(Z_Construct_UClass_AEnemyPathWaypoint, &AEnemyPathWaypoint::StaticClass, TEXT("/Script/GAM312"), TEXT("AEnemyPathWaypoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEnemyPathWaypoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
