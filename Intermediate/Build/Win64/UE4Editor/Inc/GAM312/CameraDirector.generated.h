// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_CameraDirector_generated_h
#error "CameraDirector.generated.h already included, missing '#pragma once' in CameraDirector.h"
#endif
#define GAM312_CameraDirector_generated_h

#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraDirector(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraDirector) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public:


#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraDirector)


#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_CameraDirector_h_11_PROLOG
#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_INCLASS \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_CameraDirector_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_CameraDirector_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_CameraDirector_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
