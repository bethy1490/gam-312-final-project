// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAM312_GamePickupItem_generated_h
#error "GamePickupItem.generated.h already included, missing '#pragma once' in GamePickupItem.h"
#endif
#define GAM312_GamePickupItem_generated_h

#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGamePickupItem(); \
	friend struct Z_Construct_UClass_AGamePickupItem_Statics; \
public: \
	DECLARE_CLASS(AGamePickupItem, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGamePickupItem)


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAGamePickupItem(); \
	friend struct Z_Construct_UClass_AGamePickupItem_Statics; \
public: \
	DECLARE_CLASS(AGamePickupItem, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGamePickupItem)


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGamePickupItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGamePickupItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamePickupItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamePickupItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamePickupItem(AGamePickupItem&&); \
	NO_API AGamePickupItem(const AGamePickupItem&); \
public:


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamePickupItem(AGamePickupItem&&); \
	NO_API AGamePickupItem(const AGamePickupItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamePickupItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamePickupItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGamePickupItem)


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_GamePickupItem_h_11_PROLOG
#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_INCLASS \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_GamePickupItem_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GamePickupItem_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_GamePickupItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
