// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAM312_GAM312GameMode_generated_h
#error "GAM312GameMode.generated.h already included, missing '#pragma once' in GAM312GameMode.h"
#endif
#define GAM312_GAM312GameMode_generated_h

#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_RPC_WRAPPERS
#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAM312GameMode(); \
	friend struct Z_Construct_UClass_AGAM312GameMode_Statics; \
public: \
	DECLARE_CLASS(AGAM312GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGAM312GameMode)


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAGAM312GameMode(); \
	friend struct Z_Construct_UClass_AGAM312GameMode_Statics; \
public: \
	DECLARE_CLASS(AGAM312GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AGAM312GameMode)


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAM312GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAM312GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312GameMode(AGAM312GameMode&&); \
	NO_API AGAM312GameMode(const AGAM312GameMode&); \
public:


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAM312GameMode(AGAM312GameMode&&); \
	NO_API AGAM312GameMode(const AGAM312GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAM312GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAM312GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAM312GameMode)


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PenmenHUDClass() { return STRUCT_OFFSET(AGAM312GameMode, PenmenHUDClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AGAM312GameMode, CurrentWidget); }


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_12_PROLOG
#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_INCLASS \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_GAM312GameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_GAM312GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
