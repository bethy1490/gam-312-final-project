// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAM312_StaminaPickUpItem_generated_h
#error "StaminaPickUpItem.generated.h already included, missing '#pragma once' in StaminaPickUpItem.h"
#endif
#define GAM312_StaminaPickUpItem_generated_h

#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStaminaPickUpItem(); \
	friend struct Z_Construct_UClass_AStaminaPickUpItem_Statics; \
public: \
	DECLARE_CLASS(AStaminaPickUpItem, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AStaminaPickUpItem)


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAStaminaPickUpItem(); \
	friend struct Z_Construct_UClass_AStaminaPickUpItem_Statics; \
public: \
	DECLARE_CLASS(AStaminaPickUpItem, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GAM312"), NO_API) \
	DECLARE_SERIALIZER(AStaminaPickUpItem)


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStaminaPickUpItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStaminaPickUpItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStaminaPickUpItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStaminaPickUpItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStaminaPickUpItem(AStaminaPickUpItem&&); \
	NO_API AStaminaPickUpItem(const AStaminaPickUpItem&); \
public:


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStaminaPickUpItem(AStaminaPickUpItem&&); \
	NO_API AStaminaPickUpItem(const AStaminaPickUpItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStaminaPickUpItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStaminaPickUpItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStaminaPickUpItem)


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_PRIVATE_PROPERTY_OFFSET
#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_12_PROLOG
#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_RPC_WRAPPERS \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_INCLASS \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_PRIVATE_PROPERTY_OFFSET \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_INCLASS_NO_PURE_DECLS \
	gam_312_final_project_Source_GAM312_StaminaPickUpItem_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_312_final_project_Source_GAM312_StaminaPickUpItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
