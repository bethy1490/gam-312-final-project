// Fill out your copyright notice in the Description page of Project Settings.

#include "StaminaPickUpItem.h"
#include "Penmen.h"


// Sets default values
AStaminaPickUpItem::AStaminaPickUpItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create PickupItemRoot component
	StamPickupItemRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = StamPickupItemRoot;

	//Create PickupItemMesh component and attach it to root
	StamPickupItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StamPickupItemMesh->AttachToComponent(StamPickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);

	//create PickupItemBank, attach it to root, and point to function beginOverlap and endOverlap
	StamPickupItemBox = CreateAbstractDefaultSubobject<UBoxComponent>(TEXT("PickupItemBox"));
	StamPickupItemBox->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	StamPickupItemBox->AttachToComponent(StamPickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	StamPickupItemBox->OnComponentBeginOverlap.AddDynamic(this, &AStaminaPickUpItem::OnOverlapBegin);
	StamPickupItemBox->OnComponentEndOverlap.AddDynamic(this, &AStaminaPickUpItem::OnOverlapEnd);

}

// Called when the game starts or when spawned
void AStaminaPickUpItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaminaPickUpItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStaminaPickUpItem::OnOverlapBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	//destroy object
	Destroy();
}

void AStaminaPickUpItem::OnOverlapEnd(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex)
{
}

