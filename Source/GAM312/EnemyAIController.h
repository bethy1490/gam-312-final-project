// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;


private:
	UPROPERTY()
		TArray<AActor*> Waypoints;
	

	UFUNCTION()
		ATargetPoint* GetRandomWaypoint();

	UFUNCTION()
		void GoToNextWaypoint();

	int currentWaypointIndex = 0;
	int WaypointIndexCount = 0;
	FTimerHandle TimerHandle;
	
};
