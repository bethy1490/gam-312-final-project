// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine.h"
#include "Penmen.generated.h"




UCLASS()
class GAM312_API APenmen : public ACharacter
{
	GENERATED_BODY()

private:

	//create variable for MaxStamina
	UPROPERTY(VisibleAnywhere, Category = "Stamina")
		float MaxStamina;
		
	//create variable for CurrentStamina
	UPROPERTY(VisibleAnywhere, Category = "Stamina")
		float CurrentStamina;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Sets default values for this character's properties
	APenmen();


	//function to get max stamina
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetMaxStamina();

	//function to get current stamina
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetCurrentStamina();

	//function to update stamina
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void UpdateCurrentStamina(float Stamina);

	

	//add first person camera 	
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCamera;
	//add third person camera
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* ThirdPersonCamera;

	//Add third person camera spring arm
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* SpringArm;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//declare variables

	bool IsSprinting = false;//set default value of IsSprinting to false

	bool isFirstPerson = false;//set default value of isFirstPerson to false


	FVector2D CameraMovement;
	

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//create player controller
	APlayerController* OurPlayerController;

	
	
	//Adding Movement Functions	
	void Lateral(float Value); //Class to move forward and backwards	
	void SidetoSide(float Value);//Class to move left and right	
	
	//Adding Sprint Function
	void SprintOn();//call to activate Sprint class 
	void SprintOff();//call to deactivate Sprint class

	void CameraSwitch();//function to swtich camera perspective
};
