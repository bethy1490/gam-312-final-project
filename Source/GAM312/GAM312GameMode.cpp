// Fill out your copyright notice in the Description page of Project Settings.

#include "GAM312GameMode.h"
#include "GAM312.h"
#include "Penmen.h"
#include "PenmenHUD.h"
#include "Blueprint/UserWidget.h"

void AGAM312GameMode::BeginPlay()
{
	Super::BeginPlay();

	APenmen* Player = Cast<APenmen>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (PenmenHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PenmenHUDClass);
	}
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->AddToViewport();
	}
}

AGAM312GameMode::AGAM312GameMode()
	:Super()
{
	//set default pawn class to BP_Penmen
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Penmen.BP_Penmen"));
		DefaultPawnClass = PlayerPawnClassFinder.Class;


		HUDClass = APenmenHUD::StaticClass();

}
