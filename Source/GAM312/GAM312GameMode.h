// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAM312GameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_API AGAM312GameMode : public AGameModeBase
{
	GENERATED_BODY()
		//Override beginplay from the base class
		virtual void BeginPlay() override;
	
public:
	AGAM312GameMode();

protected:
	//Create UPROPERTY for Stamina in PenmenHudClass
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))
		TSubclassOf<UUserWidget> PenmenHUDClass;

	UPROPERTY()
		class UUserWidget* CurrentWidget;


};
