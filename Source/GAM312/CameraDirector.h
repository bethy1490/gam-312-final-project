// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "CameraDirector.generated.h"

UCLASS()
class GAM312_API ACameraDirector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraDirector();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	//create player controller
	APlayerController* OurPlayerController;


	//declare variables for different camera view
	bool firstPerson = true;
	bool thirdPerson = false;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

	//Declare camera actors
	UPROPERTY(EditAnywhere)
		AActor* CameraOne;
	UPROPERTY(EditAnywhere)
		AActor* CameraTwo;
	UPROPERTY(EditAnywhere)
		AActor* PlayerCamera;
	

	//declare float variable for camera switch time
	float CameraSwitchTime;
	
	

	void setCameraOne();
	void setCameraTwo();
	void SetPlayerCamera();
	
	

	
};
