// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"
#include "GamePickupItem.generated.h"

UCLASS()
class GAM312_API AGamePickupItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGamePickupItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//create UPropertys for required components. 

	UPROPERTY(EditAnywhere)
		USceneComponent* GamePickupItemRoot;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* GamePickupItemMesh;

	UPROPERTY(EditAnywhere)
		UShapeComponent* GamePickupItemBox;

	//declare functions

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);
	
};
