// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "EnemyPathWaypoint.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_API AEnemyPathWaypoint : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	//declare public function to get pathWaypointOrder
	int GetPathWaypointOrder();

	
private:
	//declare uproperty variable to keep track of order of waypoints in path
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))	
	int PathWaypointOrder;
	
	
};
