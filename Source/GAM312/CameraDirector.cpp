// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraDirector.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();
	
	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerCamera = OurPlayerController->GetViewTarget();

}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	//declare and set variables
	const float TimeBetweenCameraChanges = 5.0f;
	const float SmoothBlendTime = 0.75f;


	//set CameraSwitchTime
	CameraSwitchTime -= DeltaTime;


	//if statement to check CameraSwitchTime is below or equal 0.0f **DISABLED**
	/*if (CameraSwitchTime <= 0.0f)
	{
		//add TimeBetweenCameraChanges to current CameraSwitchTime
		CameraSwitchTime += TimeBetweenCameraChanges;

		//Find the actor that handles control for the local player



		//If Statements to control camera switching
		if (firstPerson == false && thirdPerson == false)
		{
			if (OurPlayerController)
			{
				if ((OurPlayerController->GetViewTarget() != CameraOne) && (CameraOne != nullptr))
				{
					//cut instantly to camera one
					OurPlayerController->SetViewTarget(CameraOne);
				}
				else if ((OurPlayerController->GetViewTarget() != CameraTwo) && (CameraTwo != nullptr))
				{
					//Blend Smoothly to camera two.
					OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);
				}
			}
		}

	}
}*/

}

void ACameraDirector::setCameraOne()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != CameraOne)
		{
			OurPlayerController->SetViewTarget(CameraOne);
		}
	}
}

void ACameraDirector::setCameraTwo()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != CameraTwo)
		{
			OurPlayerController->SetViewTarget(CameraTwo);
		}
	}
}

void ACameraDirector::SetPlayerCamera()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != PlayerCamera)
		{
			OurPlayerController->SetViewTarget(PlayerCamera);
		}
	}
}

