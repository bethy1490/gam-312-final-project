// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupItem.h"
#include "DrawDebugHelpers.h"


// Sets default values
APickupItem::APickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create PickupItemRoot component
	PickupItemRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = PickupItemRoot;

	//Create PickupItemMesh component and attach it to root
	PickupItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	PickupItemMesh->AttachToComponent(PickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);

	//create PickupItemBank, attach it to root, and point to function beginOverlap and endOverlap
	PickupItemBank = CreateAbstractDefaultSubobject<UBoxComponent>(TEXT("PickupItemBank"));
	PickupItemBank->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	PickupItemBank->AttachToComponent(PickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	PickupItemBank->OnComponentBeginOverlap.AddDynamic(this, &APickupItem::OnOverlapBegin);
	PickupItemBank->OnComponentEndOverlap.AddDynamic(this, &APickupItem::OnOverlapEnd);

}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupItem::OnOverlapBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//when overlap begins display message
	DrawDebugString(GetWorld(), GetActorLocation(), "I Found an item!!!", nullptr, FColor::White, 5.0f, false);

}

void APickupItem::OnOverlapEnd(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex)
{
	//When overlap ends display message and destroy object
	DrawDebugString(GetWorld(), GetActorLocation(), "The item disappeared", nullptr, FColor::White, 5.0f, false);

	Destroy();
}

