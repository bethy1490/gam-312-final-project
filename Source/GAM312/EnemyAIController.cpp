// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);

	//WaypointIndexCount = Waypoints.Num();

	//GoToNextWaypoint();

}


void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &AEnemyAIController::GoToNextWaypoint, 1.0f, false);
	
}

ATargetPoint * AEnemyAIController::GetRandomWaypoint()
{
	auto index = FMath::RandRange(0, Waypoints.Num() - 1);
	return Cast<ATargetPoint>(Waypoints[index]);
}

void AEnemyAIController::GoToNextWaypoint()
{
	//MoveToActor(Waypoints[currentWaypointIndex]);
	//currentWaypointIndex++;
	//int Cycle = currentWaypointIndex % WaypointIndexCount;
	//if (currentWaypointIndex >= WaypointIndexCount)
	//{
	//	currentWaypointIndex = 0;
	//}
}
