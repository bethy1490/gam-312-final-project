// Fill out your copyright notice in the Description page of Project Settings.

#include "Penmen.h"
#include "GameFramework/Pawn.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "CameraDirector.h"

// Sets default values
APenmen::APenmen()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//set character orientation with movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);

	//set character movement for jumping
	GetCharacterMovement()->JumpZVelocity = 620.f;
	GetCharacterMovement()->GravityScale = 2.5f;


	//attach first person camera
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("First Person"));
	FirstPersonCamera->SetupAttachment(RootComponent);
	FirstPersonCamera->bUsePawnControlRotation = true;
	FirstPersonCamera->RelativeLocation = FVector(10, 5, 64);

	//attach SpringArm
	
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("ThirdPersonSpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
	SpringArm->TargetArmLength = 400.f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;

	//attach third person camera
	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Third Person"));
	ThirdPersonCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	ThirdPersonCamera->bUsePawnControlRotation = false;

	//set value for MaxStamina
	MaxStamina = 100.f;
	//set CurrentStamina
	CurrentStamina = MaxStamina;
}

float APenmen::GetMaxStamina()
{
	//return MaxStamina
	return MaxStamina;
}

float APenmen::GetCurrentStamina()
{
	//return CurrentStamina
	return CurrentStamina;
}

void APenmen::UpdateCurrentStamina(float Stamina)
{
	//update CurrentStamina
	CurrentStamina = CurrentStamina + Stamina;
}

// Called when the game starts or when spawned
void APenmen::BeginPlay()
{
	Super::BeginPlay();
	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
}

// Called every frame
void APenmen::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	


	//If character is sprinting then drain stamina
	if (IsSprinting)
		UpdateCurrentStamina(-DeltaTime * 0.05f * (MaxStamina));
				

	//If character is not sprinting slowly regain stamina
	if (IsSprinting != true)
		UpdateCurrentStamina(DeltaTime * 0.10f * (MaxStamina));
		
	GetCurrentStamina();//Call function to get Current Stamina
	if (CurrentStamina <= 0.02)//if CurrentStamina is below 0.02 call the SprintOn Function to update sprint 
		SprintOn();

}

// Called to bind functionality to input
void APenmen::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//add check command
	check(PlayerInputComponent);

	//add bindings

	//add lateral binding
	PlayerInputComponent->BindAxis("Lateral", this, &APenmen::Lateral);
	//add SidetoSide binding
	PlayerInputComponent->BindAxis("SidetoSide",this,&APenmen::SidetoSide);

	//add binding if shift is pressed
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APenmen::SprintOn);
	//add binding if shift is released
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APenmen::SprintOff);

	//add binding if spacebar is pressed
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APenmen::Jump);

	PlayerInputComponent->BindAction("Jump", IE_Released, this, &APenmen::StopJumping);

	//Add binding for switching camera
	PlayerInputComponent->BindAction("SwitchPerspective", IE_Pressed, this, &APenmen::CameraSwitch);

	//add binding for camera lateral movement
	PlayerInputComponent->BindAxis("CameraVerticle", this, &APenmen::AddControllerPitchInput);
	//add binding for camera sideToSide movement
	PlayerInputComponent->BindAxis("CameraSidetoSide", this, &APenmen::AddControllerYawInput);

	
}


void APenmen::Lateral(float Value)
{
	if(Controller && Value)
		//check IsSprinting boolean and increase speed
		if (IsSprinting)
			if (CurrentStamina >= 0.01)//if Current Stamina is above 0.01 update Sprint Speed
				Value *= 2;
	{		

		//get forward vector
		
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		FVector Forward = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);


		//added / 2 to decrease speed when not sprinting
		AddMovementInput(Forward, Value / 2);


	}
}

void APenmen::SidetoSide(float Value)
{
	if (Controller && Value)
		//check IsSprinting boolean and increase speed
		if (IsSprinting)
			if (CurrentStamina >= 0.01)//if Current Stamina is above 0.01 update Sprint Speed
				Value *= 2;

	{
		
	
		//get right vector
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		FVector Right = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		
		//added / 2 to decrease speed when not sprinting
		AddMovementInput(Right, Value / 2);

		
			
		
	}

}

void APenmen::SprintOn()
{	
	GetCurrentStamina();//call function to get CurrentStamina

	if (CurrentStamina >= 0.01)//If Stamina is above 0 allow sprinting to be enabled
		IsSprinting = true;
}

void APenmen::SprintOff()
{
	IsSprinting = false;
}

void APenmen::CameraSwitch()
{
	if (isFirstPerson == true)//if statement to check for camera view
	{
		FirstPersonCamera->SetActive(true);//set FirstPersonCamera to active
		ThirdPersonCamera->SetActive(false);//Set ThirdPersonCamera to deactive
		isFirstPerson = false; //update boolean
	}
	else
	{
		FirstPersonCamera->SetActive(false);//set FirstPersonCamera to deactive
		ThirdPersonCamera->SetActive(true);//Set ThirdPersonCamera to active
		isFirstPerson = true;//update boolean
	}
	
}






