// Fill out your copyright notice in the Description page of Project Settings.

#include "ComponentPawn.h"
#include "Components/InputComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "MyPawnMovementComponent.h"


// Sets default values
AComponentPawn::AComponentPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create new sphere component and set name. 

	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(40.f);
	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));

	//create new static mesh component and attach it to root component

	UStaticMeshComponent* SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	SphereMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshlComponent(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));

	//if statement to set static mesh, location and scale
	if (SphereMeshlComponent.Succeeded())
	{
		SphereMesh->SetStaticMesh(SphereMeshlComponent.Object);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		SphereMesh->SetWorldScale3D(FVector(0.8f));
	}

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	PenmenMovementComponent = CreateDefaultSubobject<UMyPawnMovementComponent>(TEXT("CustomMovements"));
	PenmenMovementComponent->UpdatedComponent = RootComponent;

}

// Called when the game starts or when spawned
void AComponentPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AComponentPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AComponentPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("Lateral", this, &AComponentPawn::Lateral);
	InputComponent->BindAxis("SidetoSide", this, &AComponentPawn::SidetoSide);

}

void AComponentPawn::Lateral(float Value)
{
	if (PenmenMovementComponent && (PenmenMovementComponent->UpdatedComponent == RootComponent))
	{
		PenmenMovementComponent->AddInputVector(GetActorForwardVector() * Value);

	}
}

void AComponentPawn::SidetoSide(float Value)
{
	if (PenmenMovementComponent && (PenmenMovementComponent->UpdatedComponent == RootComponent))
	{
		PenmenMovementComponent->AddInputVector(GetActorRightVector() * Value);

	}
}

UPawnMovementComponent* AComponentPawn::GetMovementComponent() const
{
	return PenmenMovementComponent;
}