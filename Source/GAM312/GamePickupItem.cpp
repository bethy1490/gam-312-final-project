// Fill out your copyright notice in the Description page of Project Settings.

#include "GamePickupItem.h"
#include "Enemy.h"

// Sets default values
AGamePickupItem::AGamePickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	//create PickupItemRoot component
	GamePickupItemRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = GamePickupItemRoot;

	//Create PickupItemMesh component and attach it to root
	GamePickupItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	GamePickupItemMesh->AttachToComponent(GamePickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);

	//create PickupItemBank, attach it to root, and point to function beginOverlap and endOverlap
	GamePickupItemBox = CreateAbstractDefaultSubobject<UBoxComponent>(TEXT("PickupItemBox"));
	GamePickupItemBox->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	GamePickupItemBox->AttachToComponent(GamePickupItemRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	GamePickupItemBox->OnComponentBeginOverlap.AddDynamic(this, &AGamePickupItem::OnOverlapBegin);
	GamePickupItemBox->OnComponentEndOverlap.AddDynamic(this, &AGamePickupItem::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AGamePickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGamePickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGamePickupItem::OnOverlapBegin(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	//destroy object
	Destroy();
	
}

void AGamePickupItem::OnOverlapEnd(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex)
{
	
		
}